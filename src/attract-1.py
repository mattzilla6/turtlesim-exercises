#!/usr/bin/env python2
import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

class AttractNode:
  def __init__(self, attractX, attractY):
    self.turtle_pose = None
    self.attractX = attractX
    self.attractY = attractY

    self.previousTheta = 0

    rospy.init_node('figure_8')
    velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.callback)

    vel_msg = Twist()

    r = rospy.Rate(10)
    while not rospy.is_shutdown():
      if self.atGoal():
        break

      vel_msg.linear.x = self.getLinear()
      vel_msg.angular.z = self.getAngular()

      velocity_publisher.publish(vel_msg)
      r.sleep()

  def getLinear(self):
    if not self.turtle_pose:
      return 0

    print math.fabs(self.turtle_pose.theta) - math.fabs(self.previousTheta)

    if math.fabs(math.fabs(self.turtle_pose.theta) - math.fabs(self.previousTheta)) > 0.001:
      return 0

    if self.atGoal():
      return 0

    return 1

  def atGoal(self):
    if not self.turtle_pose:
      return False

    lowX = self.attractX - 0.5
    highX = self.attractX + 0.5

    lowY = self.attractY - 0.5
    highY = self.attractY + 0.5

    inX = lowX <= self.turtle_pose.x and self.turtle_pose.x <= highX
    inY = lowY <= self.turtle_pose.y and self.turtle_pose.y <= highY

    return inX and inY


  def getAngular(self):
    if not self.turtle_pose:
      return 0
    
    if self.atGoal():
      return 0

    x = self.attractX - self.turtle_pose.x
    y = self.attractY - self.turtle_pose.y

    return math.atan2(y, x) - self.turtle_pose.theta

  def callback(self, pose):
    if self.turtle_pose:
      self.previousTheta = self.turtle_pose.theta

    self.turtle_pose = pose

def attract():
  x = int(raw_input("x: "))
  y = int(raw_input("y: "))

  attract = AttractNode(x,y)


if __name__ == '__main__':
    try:
        attract()
    except rospy.ROSInterruptException: pass
