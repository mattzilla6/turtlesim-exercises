import math

class Point:
  def __init__(self, x, y):
    self.x = x
    self.y = y

  def getDistance(self, point):
    x = math.fabs(self.x - point.x)
    y = math.fabs(self.y - point.y)

    return math.sqrt(math.pow(x, 2) + math.pow(y, 2))

  def getRadians(self, point):
    x = self.x - point.x
    y = self.y - point.y

    return math.atan2(y, x)
