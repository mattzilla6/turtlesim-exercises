#!/usr/bin/env python2
import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from turtlesim.srv import Spawn,Kill
from point import Point
import random

class Turtle:
  def __init__(self, goal, obstacle):
    self.pose = None
    self.theta = 0.0

    self.goal = goal
    self.obstacle = obstacle

    # Set up our ros node and subscribe the turtle to the pose topic
    rospy.init_node('goal_obstacle')
    velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.updatePose)

    vel_msg = Twist()

    r = rospy.Rate(10)
    while not rospy.is_shutdown():
      # Wait until we receive initial pose information
      if not self.pose:
        continue

      # Determine where we are relative to the goal. If we are there, exit
      turtleToGoalDistance = self.goal.getDistance(self.pose)
      if (turtleToGoalDistance <= 0.5):
        vel_msg.linear.x = 0
        vel_msg.angular.z = 0
        velocity_publisher.publish(vel_msg)
        break

      # Calculate a bunch of distances and angles we're going to need later
      turtleToGoalAngle = self.goal.getRadians(self.pose) - self.theta
      turtleToObstacleDistance = self.obstacle.getDistance(self.pose)
      turtleToObstacleAngle = self.obstacle.getRadians(self.pose) - self.theta

      turn = 0
      losToObstacleDistance = 0

      # Calculate the distance from the turtle's line of sight to the obstacle
      if math.fabs(math.degrees(turtleToObstacleAngle)) <= 180:
        losToObstacleDistance = math.sin(turtleToObstacleAngle) * turtleToObstacleDistance

        # If we're pointed too close to the obstacle, we have to introduce extra
        # rotation so the turtle will maneuver around it
        if math.fabs(losToObstacleDistance) <= 1.5:
          diff = 1.5 - math.fabs(losToObstacleDistance)
          try:
            turn = math.asin(diff / turtleToObstacleDistance)
          except:
            break

      vel_msg.linear.x = 1
      vel_msg.angular.z = 4*(turn + turtleToGoalAngle)

      velocity_publisher.publish(vel_msg)
      r.sleep()

  def updatePose(self, pose):
    if not self.pose:
      self.pose = Point(pose.x, pose.y)
    else:
      self.pose.x = pose.x
      self.pose.y = pose.y
      self.theta = pose.theta

def attract():
  # Get goal and obstacle coordinates from the command line
  goal = Point(input('goal x '), input('goal y '))
  obstacle = Point(input('obstacle x '), input('obstacle y '))

  # Create placeholder turtles at the goal and obstacle coordinates
  # as a visual aid
  rospy.wait_for_service('spawn')
  rospy.wait_for_service('reset')
  spawn = rospy.ServiceProxy('spawn', Spawn)
  kill = rospy.ServiceProxy('kill', Kill)

  try:
    kill('goal')
    kill('obstacle')
  except:
    pass

  spawn(goal.x, goal.y, 0, 'goal')
  spawn(obstacle.x, obstacle.y, 0, 'obstacle')

  # Set up our turtle
  turtle = Turtle(goal, obstacle)


if __name__ == '__main__':
    try:
        attract()
    except rospy.ROSInterruptException: pass
