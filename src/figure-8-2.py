#!/usr/bin/env python2
import rospy
import math
from geometry_msgs.msg import Twist
from turtlesim.msg import Pose

class Figure8Node:
  def __init__(self):
    self.turtle_start = None
    self.turtle_pose = None

    rospy.init_node('figure_8')
    velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)
    pose_subscriber = rospy.Subscriber('/turtle1/pose', Pose, self.callback)

    vel_msg = Twist()

    r = rospy.Rate(10)
    angular = 1
    while not rospy.is_shutdown():
      if self.atStart():
        angular *= -1

      vel_msg.linear.x = 1
      vel_msg.angular.z = angular

      velocity_publisher.publish(vel_msg)
      r.sleep()

  def callback(self, pose):
    if self.turtle_start is None: self.turtle_start = pose
    self.turtle_pose = pose

  def atStart(self):
    if not self.turtle_pose or not self.turtle_start:
      return False

    xDiff = math.fabs(self.turtle_start.x - self.turtle_pose.x)
    yDiff = math.fabs(self.turtle_start.y - self.turtle_pose.y)

    return yDiff < 0.5 and xDiff < 0.05

def figure8():
  figure8 = Figure8Node()


if __name__ == '__main__':
    try:
        figure8()
    except rospy.ROSInterruptException: pass
