#!/usr/bin/env python2
import rospy
from geometry_msgs.msg import Twist

def figure8():
  rospy.init_node('figure_8')
  velocity_publisher = rospy.Publisher('/turtle1/cmd_vel', Twist, queue_size=10)

  vel_msg = Twist()

  r = rospy.Rate(1)
  count = 0
  angular = -1
  while not rospy.is_shutdown():
    if (count == 0): angular *= -1
    count = (count + 1) % 6

    vel_msg.linear.x = 1
    vel_msg.angular.z = angular

    velocity_publisher.publish(vel_msg)
    r.sleep()

if __name__ == '__main__':
    try:
        figure8()
    except rospy.ROSInterruptException: pass
